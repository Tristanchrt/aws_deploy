resource "aws_instance" "strapi_dev" {

  for_each = local.variables

  ami           = each.value.ami
  instance_type = each.value.instance_type

  # VPC
  subnet_id = each.value.subnet_id

  # Security Group
  vpc_security_group_ids = each.value.vpc_security_group_ids

  # Public SSH key
  key_name = each.value.key_name

  # Tags
  tags = each.value.tags

  provisioner "remote-exec" {

    # Error cloud-init timeline see more :
    # https://stackoverflow.com/questions/42279763/why-does-terraform-apt-get-fail-intermittently
    inline = ["sudo apt-get update", "sudo apt-get update", "sudo apt-get install python -y", "echo Done!"]

    connection {
      host        = self.public_ip
      user        = var.EC2_USER
      private_key = file("${var.PRIVATE_KEY_PATH}")
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u '${var.EC2_USER}' -i '${self.public_ip},' --private-key ${var.PRIVATE_KEY_PATH} ansible/playbook/install.yml"
  }

  provisioner "local-exec" {
    command = <<EOF
        touch ansible/inventory/hosts.yml
        echo "[${each.key}]" >> hosts.yml
        echo "${self.public_ip} ansible_ssh_private_key_file=${var.PRIVATE_KEY_PATH_PEM}" >> hosts.yml
    EOF
  }

}

resource "aws_key_pair" "id_rsa" {
  key_name   = "user@laptop-cpe-"
  public_key = file(var.PUBLIC_KEY_PATH)
}
